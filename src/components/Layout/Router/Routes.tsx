import { lazy } from 'react';

import {
    Dashboard as DashboardIcon,
    Logout as LogoutIcon,
    Package,
    Profile as ProfileIcon,
    Settings as SettingsIcon,
} from '@/components/Icons';
import { AppRouter, RouterGroup } from '@/constants/routes';
import DetailPersonInfo from '@/pages/PersonInformation';
import CurrentLoginUserInfo from '@/pages/CurrentLoginUser';
import LoginUserInfo from '@/pages/LoginUserInfo';
const Home = lazy(() => import('@/pages/index'));
const Login = lazy(() => import('@/pages/SignIn'));
const Packages = lazy(() => import('@/pages/Packages'));
const CreatePackage = lazy(() => import('@/pages/Packages/Create'));
import DetailPackage from '@/pages/Packages/Detail';
const Profile = lazy(() => import('@/pages/Profile'));
const Settings = lazy(() => import('@/pages/Settings'));
const Logout = lazy(() => import('@/pages/Logout'));
const Task = lazy(() => import('@/pages/Task'));
const TaskView = lazy(() => import('@/pages/Task/Detail'));
const TaskCreate = lazy(() => import('@/pages/Task/Create'));


export const routes: AppRouter[] = [
    // { path: '/', element: <Home />, errorElement: <Error />, hidden: true, group: RouterGroup.management },
    { path: '/', element: <Home />, hidden: true, group: RouterGroup.management },
    { path: '/sign-in', element: <Login />, hidden: true, group: RouterGroup.management },
    { path: '/persondetail', element: <DetailPersonInfo />, hidden: true, group: RouterGroup.management },
    { path: '/currentLoginUserInfo', element: <CurrentLoginUserInfo />, hidden: true, group: RouterGroup.management },
    { path: '/loginuserdetail', element: <LoginUserInfo />, hidden: true, group: RouterGroup.management },
    {
        path: '/profile',
        element: <Profile />,
        group: RouterGroup.management,
        icon: <ProfileIcon />,
        label: ' Profile',
        hidden: true,
    },
    {
        path: '/packages',
        element: <Packages />,
        label: 'Gói tập',
        icon: <Package />,
        group: RouterGroup.management,
    },
    {
        path: '/package-create',
        element: <CreatePackage />,
        group: RouterGroup.management,
        hidden: true,
    },
    {
        path: '/package-detail',
        element: <DetailPackage />,
        group: RouterGroup.management,
        hidden: true,
    },
    {
        path: '/setting',
        element: <Settings />,
        group: RouterGroup.account,
        icon: <SettingsIcon />,
        label: 'Settings',
        hidden: true,
    },
    {
        path: '/tasks/',
        element: <Task />,
        label: 'Nhiệm vụ',
        group: RouterGroup.management,
        icon: <DashboardIcon />,
    },
    {
        path: '/tasks/view',
        element: <TaskView />,
        group: RouterGroup.management,
        hidden: true,
    },
    {
        path: '/tasks/create',
        element: <TaskCreate />,
        group: RouterGroup.management,
        hidden: true,
    },
    { path: '/logout', element: <Logout />, group: RouterGroup.account, icon: <LogoutIcon />, label: 'Logout' },
];
