export interface PackageResponseDto {
  id: number,
  imageUrl: string,
  packageName: string,
  descriptions: string,
  numberOfDays: number,
  numberOfSessions: number,
  packagePrice: number,
  type: string,
  sessions: [
    {
      id: number,
      title: string,
      outcome: string,
      descriptions: string,
      energyPoint: number,
      sessionItems: sessionItems[],
      branchId: null,
      branch: null,
      sessionTrainerId: null,
      sessionTrainer: null,
      isFinished: boolean
    }
  ],
  status: number,
  startDate: Date,
  endDate: Date,
  schedules: Schedule[],
  branchId: null,
  branch: {
    id: number,
    branchName: string,
    location: string,
    phone: number
  },
  packageCreator: {
    id: number,
    phone: number,
    firstName: string,
    lastName: string,
    email: string
  },
  packageTrainerId: string,
  packageTrainer: string,
  packageFollowerId: string,
  packageFollower: string
}



export interface Schedule {
    day: number;
    time: {
      hour: number,
      minute: number
    };
}
export interface sessionItems {
    title: string,
    description: string,
    imageUrl: string
}

export interface session
  {
    title: string,
    outcome: string,
    descriptions: string,
    energyPoint: number,
    sessionItems: sessionItems[]
  }
