export interface BaseResponseDto<T> {
    data: T;
    errors: unknown;
    hasError: boolean;
}
