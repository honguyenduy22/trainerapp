import { session } from "../Response/PackageResponseDto";
import { Schedule } from "./CreateSellPackageRequestDto";

export interface CreatePackageRequestDto {
    imageUrl: string,
    packageName: string,
    descriptions: string,
    numberOfDays: number,
    numberOfSessions: number,
    sessions: session[],
    startDate: Date,
    endDate: Date,
    schedules: Schedule[]
    branchId: number
  }


export interface UpdatePackageRequestDto {
    imageUrl: string,
    packageName: string;
    descriptions: string;
    numberOfDays: number;
    numberOfSessions: number;
    packagePrice: number;
    type: string;
    branchId: number;
}