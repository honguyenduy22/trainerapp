import { ColumnDef } from '@tanstack/react-table';
import { useMemo, useState } from 'react';
import { useNavigate } from 'react-router-dom';

import Button from '@/components/Button';
import TableDataList from '@/components/TableDataList';
import { EPackageType } from '@/constants/packages';

import styles from './packages.module.scss';
import TableAction from '@/components/TableAction';
import { formatNumber } from '@/utils/common';
import SearchBox from '@/components/SearchBox/SearchBox';
import { executeDeleteWithBody } from '@/utils/http-client';
import toast from 'react-hot-toast';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import { BaseResponseDto } from '@/interfaces/Response/BaseResponseDto';
import { PackageResponseDto } from '@/interfaces/Response/PackageResponseDto';
import Status from '@/components/Status';
import { ETaskType } from '@/constants/task';
import TaskType from '@/components/TaskType';

const Packages = () => {
    let packageId = 0;
    const [searchTerm, setSearchTerm] = useState<string>("");
    const [refreshTable, setRefreshTable] = useState(false);
    const options = {
        title: 'XÁC NHẬN',
        message: 'Bạn có muốn xóa gói tập?',
        buttons: [
          {
            label: 'Yes',
            onClick: async () => {
                try {
                    
                    const { data }: { data: BaseResponseDto<string> } = await executeDeleteWithBody(`/api/DemoPackage/${packageId}`);
                    if(data.hasError === false){
                        toast.success('Xóa gói tập thành công');
                    }
                    else{
                        toast.error('Xóa gói tập thất bại');
                    }
                    setRefreshTable(!refreshTable);
                } catch (error) {
                    // toast.error('Delete Package Error!');
                } finally {
                    setSearchTerm("");
                    // setIsLoading(false);
                }
            }
          },
          {
            label: 'No',
            onClick: () => {}
          }
        ],
        closeOnEscape: true,
        closeOnClickOutside: true,
        keyCodeForClose: [8, 32],
        overlayClassName: "overlay-custom-class-name"
      };

    const handleDeleteClick = (id: number) => {
        packageId = id;
        confirmAlert(options);
    };
    
    const handleCreateSellPackage = (id: number) => {
        navigate(`/consultant-package/create?id=${id}`,
        {
            state: { 
                "id": id
                }
              } 
        );
    };
    const getStatus = (statusText: number) => {
        if(statusText == 1){
            return "InProcess"
        }
        else if(statusText == 2)
        {
            return "Finished"
        }
    }
    const cols = useMemo<ColumnDef<PackageResponseDto>[]>(
        () => [
            { header: 'Tên giáo án', accessorKey: 'packageName' },
            { header: 'Huấn luyện viên', accessorKey: 'packageTrainer' },
            { header: 'Khách hàng', accessorKey: 'packageFollower' },
            { header: 'Quản lý', accessorKey: 'packageCreator.firstName'
                ,cell: (x) => (
                    x.cell.row.original.packageCreator.lastName + " " + x.cell.row.original.packageCreator.firstName
                ),
            },
            {
                header: 'Trạng thái',
                accessorKey: 'status',
                cell: (value ) => (
                    <TaskType text={getStatus(value.getValue() as number) as string} type={getStatus(2) as ETaskType} />
                ),
            },
            {
                header: 'Thao tác',
                cell: (x) => (
                    <TableAction
                        onEditClick={()=>
                                {
                                navigate('/package-detail', {
                                    state: { 
                                        "url": x.cell.row.original.imageUrl,
                                        "id": x.cell.row.original.id,
                                        "packageName": x.cell.row.original.packageName,
                                        "descriptions": x.cell.row.original.descriptions,
                                        "numberOfDays": x.cell.row.original.numberOfDays,
                                        "numberOfSessions": x.cell.row.original.numberOfSessions,
                                        "packagePrice": x.cell.row.original.packagePrice,
                                        "type": x.cell.row.original.type,
                                        "branchId": x.cell.row.original.branch.id,
                                        }
                                      } 
                                  );
                                }
                        }
                        onDeleteClick={() => handleDeleteClick(x.cell.row.original.id)}
                    />
                ),  
            },
        ],
        [searchTerm, refreshTable],
    );

    const navigate = useNavigate();
    const handleCreatePackageClick = () => {
        navigate('/package-create');
    };
    const handleChangeSearchBox = (x : any) => {
        console.log(x.target.value);
        setSearchTerm(x.target.value);
    }

    return (
        <div className={styles.container}>
            <div className={styles.header}>
                <div className={styles.title}>
                    <span>Gói tập </span>
                </div>
                <SearchBox 
                onChange={(x) => handleChangeSearchBox(x)} 
                value={searchTerm}/>
                <div>
                    <Button
                        content={<span>Tạo giáo án</span>}
                        className={styles.button}
                        onClick={handleCreatePackageClick}
                    />
                </div>
            </div>
            <div className={styles.table}>
                <TableDataList cols={cols} path={`/api/Package?query=${searchTerm}`} key={searchTerm} isRefresh={refreshTable}/>
            </div>
        </div>
    );
};

export default Packages;
